import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class Solution {
	
	class Game {
		Integer number;
		
		List<Integer> getDigits(){
			String digits = number.toString();
			List<Integer> digitList = new ArrayList<Integer>();
			for(int i = 0; i < digits.length(); i++){
				Character character = digits.charAt(i);
				digitList.add(Integer.parseInt(character.toString()));
			}
			return digitList;
		}
		
		void playGame(){
			List<Integer> digits = getDigits();
			List<Integer> divisors = new ArrayList<Integer>();
			for(Integer candidate: digits){
				if(candidate != 0){
					if(number % candidate == 0){
						divisors.add(candidate);
					}
				}
			}
			System.out.println(divisors.size());
		}
	}
	
	public static void main(String[] args) {
		Solution sol = new Solution();
		List<Game> games = sol.readInput();
		for(Game game: games){
			game.playGame();
		}
    }

	private List<Game> readInput() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		List<Game> games = new ArrayList<Game>();
		try {
			String line = br.readLine(); //Skip first line
			while((line = br.readLine()) != null){
				if(line.isEmpty()) break;
				Game game = new Game();
				game.number = Integer.parseInt(line);
				games.add(game);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return games;
	}
}
