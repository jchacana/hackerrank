import java.util.ArrayList;
import java.util.List;


public class Solution {
	
	public static void main(String args[]){
		
		Node<String> nodeA, nodeB, nodeC, nodeD, nodeE;
		nodeA = new Node<String>("a");
		nodeB = new Node<String>("b");
		nodeC = new Node<String>("c");
		nodeD = new Node<String>("d");
		nodeE = new Node<String>("e");
		
		nodeA.addEdge(nodeB);
		nodeA.addEdge(nodeD);
		
		nodeB.addEdge(nodeC);
		nodeB.addEdge(nodeE);
		
		nodeC.addEdge(nodeD);
		nodeC.addEdge(nodeE);
		
//		nodeD.addEdge(nodeB);
		
		try {
			nodeA.dependencyResolve(new ArrayList<Node<String>>());
			for(Node<String> node: nodeA.resolved){
				System.out.print(node);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

}

class Node<T extends Comparable<T>> implements Comparable<Node<T>>{
	T value;
	List<Node<T>> edges;
	List<Node<T>> resolved;
	
	
	Node(T value){
		this.value = value;
		this.edges = new ArrayList<Node<T>>();
		this.resolved = new ArrayList<Node<T>>();
	}
	
	void addEdge(Node<T> node){
		this.edges.add(node);
	}
	
	List<Node<T>> dependencyResolve(List<Node<T>> unresolved) throws Exception{
		unresolved.add(this);
		for(Node<T> node: edges){
			if(!resolved.contains(node)){
				if(unresolved.contains(node))
					throw new Exception("Dependencia circular detectada " + this + "-" + node);
				resolved.addAll(node.dependencyResolve(unresolved));
			}
		}
		resolved.add(this);
		unresolved.remove(this);
		return resolved;
	}
	
	@Override
	public String toString() {
		return value.toString();
	}
	
	@Override
	public int compareTo(Node<T> o) {
		return this.value.compareTo(o.value);
	}
	
}
