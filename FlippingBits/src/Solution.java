import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class Solution {

	class Case{
		long number;
		
		public Long flip(){
			String bin = Long.toBinaryString(~number).substring(32);
			Long returnNumber = Long.parseLong(bin, 2);
			return returnNumber;
		}
	}
	
	public static void main(String[] args){
		Solution sol = new Solution();
		List<Case> cases = sol.readInput();
		for(Case test: cases){
			System.out.println( test.flip());
		}
	}
	
	private List<Case> readInput(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		List<Case> cases = new ArrayList<Case>();
		try {
			int linesToRead = Integer.parseInt(br.readLine()); //Skip first line
			String line = "";
			do{
				line = br.readLine();
				Case testCase = new Case();
				testCase.number = Long.parseLong(line);
				cases.add(testCase);
			}while(--linesToRead > 0 || line == null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return cases;
	}
}
