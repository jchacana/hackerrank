import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;


public class Solution {

	public static void main(String[] args) {
		Solution sol = new Solution();
		sol.doCase();
	}
	
	private void doCase(){
		Scanner in = new Scanner(System.in);
		int cases = in.nextInt();
		do{
			int diff = in.nextInt();
			int product = in.nextInt();
			System.out.println(diffProd(diff, product));
		}while(--cases > 0);
	}

	private String diffProd(int diff, int product) {
		if(diff < 0) return "0";
		Set<String> elSet = new HashSet<String>();
		double sqr = Math.sqrt(Math.pow(diff, 2) + 4*product);
		
		double[] tempAnswer = new double[2];
		tempAnswer[0] = ((diff+sqr)/2);
		tempAnswer[1] = ((-diff+sqr)/2);
		elSet.add(""+tempAnswer[0]+","+tempAnswer[1]);
		
		tempAnswer = new double[2];
		tempAnswer[0] = ((diff+sqr)/2);
		tempAnswer[1] = ((-diff-sqr)/2);
		elSet.add(""+tempAnswer[0]+","+tempAnswer[1]);
		
		tempAnswer = new double[2];
		tempAnswer[0] = ((diff-sqr)/2);
		tempAnswer[1] = ((-diff+sqr)/2);
		elSet.add(""+tempAnswer[0]+","+tempAnswer[1]);
		
		tempAnswer = new double[2];
		tempAnswer[0] = ((diff-sqr)/2);
		tempAnswer[1] = ((-diff-sqr)/2);
		elSet.add(""+tempAnswer[0]+","+tempAnswer[1]);
		
		return ""+elSet.size();
	}

}
