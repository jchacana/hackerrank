import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class Solution {
	class Case{
		int map[][];
		Case(int mapSize){
			map = new int[mapSize][mapSize];
		}
		
		void putMap(int index, int line[]){
			map[index] = line;
		}
		
		void readLine(int index, String line){
			int arrayLine[] = new int[line.length()];
			int i = 0;
			for(char c:line.toCharArray()){
				arrayLine[i] = Character.digit(c, 10);
				i++;
			}
			putMap(index, arrayLine);
		}
		
		void printMap(char aMap[][]){
			for(int i = 0; i < aMap.length; i++){
				for(int j = 0; j < aMap[i].length; j++){
					System.out.print(aMap[i][j]);
				}
				System.out.print("\n");
			}
		}
		
		void printMap(int aMap[][]){
			for(int i = 0; i < aMap.length; i++){
				for(int j = 0; j < aMap[i].length; j++){
					System.out.print(aMap[i][j]);
				}
				System.out.print("\n");
			}
		}
		
		void printCavities(){
//			printMap(map);
			char theMap[][] = new char[map.length][map.length];
			for(int i = 0; i < map.length; i++){
				for(int j = 0; j < map[i].length; j++){
					if(isCavity(i,j)){
						theMap[i][j] = 'X'; 
					} else {
						theMap[i][j] = Character.forDigit(map[i][j], 10);
					}
				}
			}
			printMap(theMap);
		}
		
//		1112
//		1912
//		1892
//		1234
		private boolean isCavity(int i, int j) {
			if(i == 0)return false;
			if(j == map[i].length) return false;
			int cell = map[i][j];
			int bordes[] = new int[4];
			bordes[0] = i-1 < 0?0:i-1;
			bordes[1] = i+1 >= map.length?map.length-1:i+1;
			bordes[2] = j-1 < 0?0:j-1;
			bordes[3] = j+1 >= map[i].length? map[i].length-1:j+1;
			if(cell > map[bordes[0]][j] && 
				cell > map[bordes[1]][j] &&
				cell > map[i][bordes[2]] &&
				cell > map[i][bordes[3]])
				return true;
			return false;
		}
	}
	
	public static void main(String[] args){
		Solution sol = new Solution();
		List<Case> cases = sol.readInput();
		for(Case test: cases){
			test.printCavities();
		}
	}
	
	private List<Case> readInput(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		List<Case> cases = new ArrayList<Case>();
		try {
			int linesToRead = Integer.parseInt(br.readLine()); //Skip first line
			int mapSize = linesToRead;
			String line = "";
			int index = 0;
			Case testCase = new Case(mapSize);
			do{
				line = br.readLine();
				testCase.readLine(index, line);
				//cases.add(testCase);
				index++;
			}while(--linesToRead > 0 || line == null);
			cases.add(testCase);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return cases;
	}
}
