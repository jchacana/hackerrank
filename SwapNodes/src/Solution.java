import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;


public class Solution {
	
	class Tree<T>{
		Tree<T> leftChild;
		Tree<T> rightChild;
		Tree<T> parent;
		T value;
		
		public Tree() {
			
		}
		
		public Tree(T value){
			this.value = value;
		}
		
		@Override
		public String toString() {
			return value.toString();
		}
		
		void addLeftNode(T value){
			if(isValid(value)){
				leftChild = new Tree<T>(value);
				leftChild.parent = this;
			}
		}
		
		private boolean isValid(T value) {
			if( value instanceof Integer){
				return (Integer)value > 1;
			}
			return false;
		}

		void addRightNode(T value){
			if(isValid(value)){
				rightChild = new Tree<T>(value);
				rightChild.parent = this;
			}
		}
		
		void inOrder(){
			if(leftChild != null) 
				leftChild.inOrder();
			System.out.print(value + " ");
			if(rightChild != null)
				rightChild.inOrder();
		}

		public void doSwapAt(int level, int nodes) {
			List<Integer> levels = new LinkedList<Integer>();
			int i = 1;
			while(i*level <= nodes){
				levels.add(i*level);
				i++;
			}
			for(Integer currentLevel: levels){
				Queue<Tree<T>> queue = visitLevel(currentLevel);
				doSwap(queue);
			}
			this.inOrder();
			System.out.print("\n");
		}

		private void doSwap(Queue<Tree<T>> currentLevel) {
			Tree<T> tree = null;
			while((tree = currentLevel.poll()) != null){
				tree.swap();
			}
		}

		private void swap() {
			Tree<T> temporal;
			temporal = this.leftChild;
			this.leftChild = this.rightChild;
			this.rightChild = temporal;
			
		}

		private Queue<Tree<T>> visitLevel(int level) {
			Queue<Tree<T>> queue = new LinkedList<Tree<T>>();
			Tree<T> tree = this;
			queue.offer(this);
			int currentLevel = 1;
			while(!queue.isEmpty()){
				int size = queue.size();
				if(currentLevel == level) return queue;
				while(size > 0){
					tree = queue.poll();
					if(tree.leftChild != null)
						queue.offer(tree.leftChild);
					if(tree.rightChild != null)
						queue.offer(tree.rightChild);
					size--;
				}
				currentLevel++;
			}
			return queue;
		}

	}
	
	public static void main(String argsr[]){
		Solution sol = new Solution();
		sol.readInput();
	}

	private void readInput(){
		Scanner in = new Scanner(System.in);
		int nodes = in.nextInt();
		int depth = nodes;
		Tree<Integer> tree = new Tree<Integer>(1);
		Queue<Tree<Integer>> queue = new LinkedList<Tree<Integer>>();
		queue.offer(tree);
		Tree<Integer> masterTree = tree;
		int leftNode = 0;
		int rightNode = 0;
		while((tree = queue.poll()) != null && nodes > 0){
			leftNode = in.nextInt();
			rightNode = in.nextInt();
			tree.addLeftNode(leftNode);
			tree.addRightNode(rightNode);
			if(tree.leftChild != null)
				queue.offer(tree.leftChild);
			if(tree.rightChild != null)
				queue.offer(tree.rightChild);
			nodes--;
		}
		int swap = in.nextInt();
		do{
			int level = in.nextInt();
			masterTree.doSwapAt(level, depth);
		}while(--swap > 0);
	}
}
