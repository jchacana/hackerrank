import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class Solution {

	public class Case {
		Integer cycles;
		
		public Long growth(){
			Long size = 1L;
			for(int i = 1; i <= cycles; i++){
				if(i % 2 == 0){
					size += 1;
				} else {
					size *= 2;
				}
			}
			return size;
		}
	}

	public static void main(String[] args){
		Solution sol = new Solution();
		List<Case> testCases = sol.readInput();
		for(Case test: testCases){
			System.out.println(test.growth());
		}
	}

	private List<Case> readInput() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		List<Case> games = new ArrayList<Case>();
		try {
			String line = br.readLine(); //Skip first line
			while((line = br.readLine()) != null){
				if(line.isEmpty()) break;
				Case testCase = new Case();
				testCase.cycles = Integer.parseInt(line);
				games.add(testCase);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return games;
	}
}
