import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class Solution {

	public class Case {
		String string;

		public Integer delete() {
			StringBuffer strBuffer = new StringBuffer();
			Character current = '\0';
			for(Character character: string.toCharArray()){
				if(!current.equals(character)){
					strBuffer.append(current);
				}
				current = character;
			}
			return string.length() - strBuffer.length();
		}
	}

	public static void main(String[] args){
		Solution sol = new Solution();
		List<Case> testCases = sol.readInput();
		for(Case test: testCases){
			System.out.println(test.delete());
		}
	}

	private List<Case> readInput() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		List<Case> games = new ArrayList<Case>();
		try {
			String line = br.readLine(); //Skip first line
			while((line = br.readLine()) != null){
				if(line.isEmpty()) break;
				Case testCase = new Case();
				testCase.string = line;
				games.add(testCase);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return games;
	}
}
