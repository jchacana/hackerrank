import java.util.Scanner;


public class Duplicates {

	public static void main(String[] args) {
		Duplicates d = new Duplicates();
		String word = d.readInput();
		System.out.println(d.duplicate(word));
	}
	
	private String duplicate(String word) {
		StringBuffer sb = new StringBuffer();
		int index = 1;
		for(Character character: word.toCharArray()){
			if(sb.toString().indexOf(character) == -1)
				sb.append(character);
		}
		
		return sb.toString();
	}

	private String readInput(){
		Scanner in = new Scanner(System.in);
		String word = in.next();
		return word;
	}
}
