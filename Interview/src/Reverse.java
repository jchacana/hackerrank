import java.util.Scanner;


public class Reverse {
	public static void main(String[] args) {
		Reverse reverse = new Reverse();
		String word = reverse.readInput();
		word = reverse.doReverse(word);
		System.out.println(word);
	}
	
	private String doReverse(String word) {
		StringBuffer newString = new StringBuffer();
		for(int i = word.length()-1; i>= 0; i--){
			newString.append(word.charAt(i));
		}
//		newString.append("\0");
		return newString.toString();
		
	}

	private String readInput(){
		Scanner in = new Scanner(System.in);
		String word = in.next();
		word = word + "\0";
		return word;
	}
}
