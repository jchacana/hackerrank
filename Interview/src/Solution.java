import java.util.Scanner;


public class Solution {

	public static void main(String args[]){
		Solution sol = new Solution();
		String word = sol.readInput();
		System.out.println(sol.isUnique(word));
	}
	
	private Boolean isUnique(String str) {
		int index = 1;
		for(Character character:str.toCharArray()){
			if(str.substring(index).indexOf(character) != -1) return false;
			index++;
		}
		return true;
	}

	private String readInput(){
		Scanner in = new Scanner(System.in);
		String word = in.next();
		return word;
	}
}
