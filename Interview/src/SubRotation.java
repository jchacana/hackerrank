
public class SubRotation {

	public static void main(String[] args) {
		
		SubRotation sub = new SubRotation();
		System.out.println(sub.isRotation("waterbottle", "erbottlewat"));
	}
	
	public Boolean isRotation(String s1, String s2){
		if(s1.length() != s2.length()) return false;
		String s3 = s2+s2;
		return s3.contains(s1);
	}
}
