import java.util.Scanner;


public class ReplaceSpace {
	public static void main(String[] args) {
		ReplaceSpace rs = new ReplaceSpace();
		String word = rs.readInput();
		System.out.println(rs.replaceSpace(word));
	}
	
	private String replaceSpace(String word) {
		StringBuffer sb = new StringBuffer();
		for(Character character: word.toCharArray()){
			if(character == ' ')
				sb.append("%20");
			else
				sb.append(character);
		}
		return sb.toString();
	}

	private String readInput(){
		Scanner in = new Scanner(System.in);
		String word1 = in.nextLine();
		return word1;
	}
}
