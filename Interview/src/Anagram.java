import java.util.Arrays;
import java.util.Scanner;


public class Anagram {
	
	private String word1;
	private String word2;
	
	public static void main(String[] args) {
		Anagram anagram = new Anagram();
		anagram.readInput();
		System.out.println(anagram.isAnagram());
	}
	
	private Boolean isAnagram() {
		if(word1.length() != word2.length()) return false;
		char [] lword1 = word1.toCharArray();
		char [] lword2 = word2.toCharArray();
		Arrays.sort(lword1);
		Arrays.sort(lword2);
		return Arrays.equals(lword1, lword2);
	}

	private void readInput(){
		Scanner in = new Scanner(System.in);
		word1 = in.next();
		word2 = in.next();
	}
}
