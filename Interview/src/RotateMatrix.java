
public class RotateMatrix {
	public static void main(String[] args) {
		RotateMatrix rot = new RotateMatrix();
		int[][] matrix = getMatrix();
		rot.printMatrix(matrix);
		matrix = rot.rotate(matrix, matrix.length);
		System.out.println("Rotate");
		rot.printMatrix(matrix);
	}
	
	private void printMatrix(int[][] matrix) {
		for(int i = 0; i < matrix.length; i++){
			for(int j = 0; j < matrix[i].length; j++){
				System.out.printf("[%2d]",matrix[i][j]);
			}
			System.out.println("");
		}
		
	}

	private static int[][] getMatrix() {
		int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
		return matrix;
	}

	public int[][] rotate(int [][] matrix, int size){
		int temp = 0;
		for(int layer = 0; layer < size/2; layer++){
			int first = layer;
			int last = size - 1 - layer;
			for(int j = first; j < last; j++){
				int offset = j - first;
				temp = matrix[first][j];
				matrix[first][j] = matrix[last-offset][first];
				
				matrix[last-offset][first] = matrix[last][last-offset];
				
				matrix[last][last-offset] = matrix[j][last];
				
				matrix[j][last] = temp;
			}
			
		}
		return matrix;
	}
}
