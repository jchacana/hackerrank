import java.util.Arrays;


public class SetZero {
	public static void main(String[] args) {
		SetZero zero = new SetZero();
		int [][] matrix = zero.getMatrix();
		zero.printMatrix(matrix);
		System.out.println("zerooo");
		zero.printMatrix(zero.setZero(matrix));
	}
	
	private void printMatrix(int[][] matrix) {
		for(int i = 0; i < matrix.length; i++){
			for(int j = 0; j < matrix[i].length; j++){
				System.out.printf("[%2d]",matrix[i][j]);
			}
			System.out.println("");
		}
		
	}
	
	private int[][] getMatrix(){
		int[][] matrix = {{1,2,3,4},{5,6,7,0},{9,10,11,12},{0,14,15,16}};
		return matrix;
	}
	
	public int[][] setZero(int [][] matrix){
		int copia[][] = makeCopy(matrix);
		for(int i = 0; i < matrix.length; i++){
			for(int j = 0; j < matrix[i].length; j++){
				if(matrix[i][j] == 0)
					copia = makeZero(copia, i, j);
			}
		}
		return copia;
	}

	private int[][] makeCopy(int[][] matrix) {
		int[][] copia = new int[matrix.length][matrix[0].length];
		for(int i = 0; i < matrix.length; i++){
			copia[i] = Arrays.copyOf(matrix[i], matrix[i].length);
		}
		return copia;
	}

	private int[][] makeZero(int[][] copia, int row, int col) {
		for(int i = 0; i < copia[row].length; i++){
			copia[row][i] = 0;
		}
		for(int i = 0; i < copia.length; i++){
			copia[i][col] = 0;
		}
		return copia;
	}

}
