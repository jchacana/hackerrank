import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class Solution {

	class Case{
		int min;
		int max;
		
		public int xor(){
			int res = min ^ max;
			int a = 0;
			while(Math.pow(2, a) < res)a++;
			return (int)Math.pow(2, a) -1;
		}
	}
	
	public static void main(String[] args){
		Solution sol = new Solution();
		List<Case> cases = sol.readInput();
		for(Case test: cases){
			System.out.println( test.xor());
		}
	}
	
	private List<Case> readInput(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		List<Case> cases = new ArrayList<Case>();
		try {
			String line = "";
			do{
				Case testCase = new Case();
				line = br.readLine();
				testCase.min = Integer.parseInt(line);
				line = br.readLine();
				testCase.max = Integer.parseInt(line);
				cases.add(testCase);
			}while(line == null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return cases;
	}
}
