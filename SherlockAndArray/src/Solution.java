import java.util.Scanner;


public class Solution {

	class Case{

		int array[];
		int total;
		
		
		public String existElement(){
			if(array.length == 1) return "YES";
			int sumLeft = 0;
			int sumRight = total;
			for(int i = 0; i < array.length; i++){
				if(i == 0) {
					sumLeft = 0;
					sumRight = total;
				}
				else if(i == array.length -1){ 
					sumRight = 0;
					sumLeft = total;
				} else {
					sumLeft+=array[i-1];
					sumRight=total-sumLeft-array[i];
				}
				if(sumLeft == sumRight) return "YES";
			}
			return "NO";
		}
	}
	
	public static void main(String[] args){
		Solution sol = new Solution();
		sol.readInput();
	}
	
	private void readInput(){
		Scanner in = new Scanner(System.in);
		int testCases = in.nextInt();
		int elements = 0;
		int index = 0;
		int array[];
		int sum = 0;
		do{
			sum = 0;
			index = 0;
			elements = in.nextInt();
			array = new int[elements];
			do{
				array[index] = in.nextInt();
				sum+=array[index];
				index++;
			}while(--elements > 0);
			Case caso = new Case();
			caso.array = array;
			caso.total = sum;
			System.out.println(caso.existElement());
		}while(--testCases > 0);
	}
}
