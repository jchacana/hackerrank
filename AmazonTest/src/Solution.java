import java.io.*;
import java.util.*;


public class Solution {
    static class Part {
        public Part(){}
        
        public String getName() { 
            return "X";
        }
    }

    static class Operation {
        private String name_;
        
        public Operation(final String name) { 
            name_ = name; 
        }
        public String getName() { return name_; }
        public void operate(Part p) {
            System.out.println("Operation " + name_ + " on part " + p.getName());
        }
    }

    static class StepManager {
     
    	List<String> called = new ArrayList<String>();
    	private Map<String,List<String>> operations = new HashMap<String,List<String>>();
        
        public void addOperation(final Operation operation, final String[] prerequisites) {
            // Add your implementation here. This method is called
            // once for each type of operation during setup.
        	if(!operations.containsKey(operation.getName()))
        		operations.put(operation.getName(), Arrays.asList(prerequisites));
        }
        public void performOperation(String operationName, Part p) {
            // Add your implementation here. When this method is called,
            // you must call the 'operate()' method for the named operation,
            // as well as all prerequisites for that operation.
        	List<String> prerequisites = operations.get(operationName);
        	for(String requisite: prerequisites){
        		if(!called.contains(requisite)){
        			called.add(requisite);
        			performOperation(requisite, p);
        		}
        	}
        	new Operation(operationName).operate(p);
        }
    }

    public static void main(String[] args) throws Exception {
        StepManager manager = new StepManager();

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String s;
        while ((s = in.readLine()) != null && s.length() != 0) {
            if (s.startsWith("#")) {
                continue;
            }
            String[] parts = s.split(",");
            manager.addOperation(new Operation(parts[0]),  Arrays.copyOfRange(parts, 1, parts.length));
        }

        manager.performOperation("final", new Part());
    }
}