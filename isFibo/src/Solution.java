import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Solution {

	List<Long> fiboList = new ArrayList<Long>();
	
	class Case {
		Long number;
		
		boolean isFibo(){
			Integer fibo = 0;
			Long fibonacci = getFibonacci(fibo);
			while(fibonacci.compareTo(number) <= 0){
				if(number.equals(fibonacci)){
					return true;
				}
				else{
					fibo++;
					fibonacci = getFibonacci(fibo);
				}
			}
			return false;
		}
		
		Long getFibonacci(Integer n){
			if(fiboList.size() == 0){
				fiboList.add(0L);
				fiboList.add(1L);
			}
			if(n >= 2){
				if(fiboList.size() - 1 >= n){
					return fiboList.get(n);
				} else {
					fiboList.add(getFibonacci(n -1) + getFibonacci(n - 2));
				}
			}
			return fiboList.get(n);
		}
	}
	
	public static void main(String[] args){
		Solution sol = new Solution();
		List<Case> testCases = sol.readInput();
		int testCase = 0;
		for(Case test: testCases){
			String result = test.isFibo()?"IsFibo":"IsNotFibo";
			System.out.println(result);
			testCase++;
		}
	}
	
	private List<Case> readInput(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		List<Case> games = new ArrayList<Case>();
		try {
			String line = br.readLine(); //Skip first line
			while((line = br.readLine()) != null){
				if(line.isEmpty()) break;
				Case testCase = new Case();
				testCase.number = Long.parseLong(line);
				games.add(testCase);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return games;
	}
}
